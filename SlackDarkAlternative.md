0. Originally ound here (ignore this):

https://gist.github.com/DrewML/0acd2e389492e7d9d6be63386d75dd99#gistcomment-1981178

1. Set the permissions so you can edit:

```
sudo chmod ugo+w /Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static/index.js
```

2. Follow this readme: https://github.com/widget-/slack-black-theme

3. Okay, and then add this (hack upon hack upon hack):

```
   let customCustomCSS = `
   :root {
     /* Modify these to change your theme colors: */
     --primary: #CCC;
     --text: #999;
     --background: #222;
     --background-elevated: #444;
       .internal_member_link {
         --background-color: darkolivegreen;
       }
   }
       .ts_tip_float {
         background-color: var(--background) !important;
       }
       .app_preview_link_slug, .internal_member_link, .internal_user_group_link, ts-mention {
         background-color: var(--background) !important;
       }`
```
